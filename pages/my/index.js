var e = require("../../@babel/runtime/helpers/regeneratorRuntime"),
  n = require("../../@babel/runtime/helpers/asyncToGenerator"),
  t = require("../../utils/$"),
  r = t.GetUser,
  a = t.post,
  s = t.uploadimg;
getApp();
Page({
  data: {},
  jumpTo: function (e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })
  },
  tipTo: function (e) {
    wx.showToast({
      icon: "none",
      title: "教程内容录制中...."
    })
  },
  onLoad: function () {
    var t = this;
    return n(e().mark((function n() {
      var s;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return e.next = 2, r();
          case 2:
            return e.next = 4, a("MyIndex", {});
          case 4:
            s = e.sent, t.setData(s.Data);
          case 6:
          case "end":
            return e.stop()
        }
      }), n)
    })))()
  },
  phone: function (t) {
    return n(e().mark((function n() {
      var t, r;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return e.next = 2, a("GetExt", {
              title: "客服电话"
            });
          case 2:
            if (t = e.sent, r = t.Data.Content) {
              e.next = 6;
              break
            }
            return e.abrupt("return");
          case 6:
            wx.makePhoneCall({
              phoneNumber: r
            });
          case 7:
          case "end":
            return e.stop()
        }
      }), n)
    })))()
  },
  scan: function (t) {
    return n(e().mark((function n() {
      var t, r;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return e.next = 2, wx.scanCode({
              scanType: ["qrCode"]
            });
          case 2:
            return t = e.sent, e.next = 5, a("ScanQrCode", {
              code: t.result
            });
          case 5:
            r = e.sent, wx.showToast({
              icon: "none",
              title: r.Msg
            }), getApp().onLaunch();
          case 8:
          case "end":
            return e.stop()
        }
      }), n)
    })))()
  },
  upload: function (t) {
    return n(e().mark((function n() {
      var t;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return e.next = 2, s();
          case 2:
            return t = e.sent, e.next = 5, a("ModUserImg", {
              url: t
            });
          case 5:
            getApp().onLaunch();
          case 6:
          case "end":
            return e.stop()
        }
      }), n)
    })))()
  },
  wxzf: function (t) {
    var r = this;
    return n(e().mark((function n() {
      var t, s, u, c;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return t = r.data, e.next = 3, a("addvip", t);
          case 3:
            return s = e.sent, e.next = 6, a("wxzf", s.Data);
          case 6:
            return u = e.sent, e.next = 9, wx.requestPayment(u.Data);
          case 9:
            return e.sent, e.next = 12, a("CheckOrder", s.Data);
          case 12:
            c = e.sent, wx.showModal({
              showCancel: !1,
              content: c.Msg
            }).then((function (e) {
              e.confirm && getApp().onLaunch()
            }));
          case 14:
          case "end":
            return e.stop()
        }
      }), n)
    })))()
  }
});