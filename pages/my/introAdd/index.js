var e = require("../../../@babel/runtime/helpers/regeneratorRuntime"),
  t = require("../../../@babel/runtime/helpers/asyncToGenerator"),
  r = require("../../../utils/$"),
  n = r.GetUser,
  a = r.Confirm,
  u = r.post;
getApp();
Page({
  data: {},
  setdate: function (e) {
    var t = e.detail.value;
    this.setData({
      "u.Birthday": t
    })
  },
  bindViewTap: function () {},
  submit: function (r) {
    return t(e().mark((function t() {
      var n, i;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            if (!(n = r.detail.value).name) {
              e.next = 8;
              break
            }
            return e.next = 4, u("moduserdetail", n);
          case 4:
            return i = e.sent, e.next = 7, a(i.Msg);
          case 7:
            getApp().onLaunch();
          case 8:
          case "end":
            return e.stop()
        }
      }), t)
    })))()
  },
  onLoad: function () {
    return t(e().mark((function t() {
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return e.next = 2, n();
          case 2:
          case "end":
            return e.stop()
        }
      }), t)
    })))()
  }
});