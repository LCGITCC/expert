var e, t = require("../../../@babel/runtime/helpers/regeneratorRuntime"),
  n = require("../../../@babel/runtime/helpers/asyncToGenerator"),
  r = (e = require("/@vant/weapp/dialog/dialog")) && e.__esModule ? e : {
    default: e
  };
var a = require("../../../utils/$"),
  i = a.GetUser,
  s = a.post;
getApp();
Page({
  data: {
    key: "",
    page: 0,
    size: 20,
    id: 0,
    list: [],
    prewshow: !1
  },
  search: function (e) {
    var t = this;
    this.setData({
      key: e.detail.value,
      page: 0,
      list: []
    }, (function () {
      t.scroll()
    }))
  },
  copy: function (e) {
    var t = e.currentTarget.dataset.v;
    wx.setClipboardData({
      data: t
    })
  },
  beforeClose: function (e) {
    return new Promise((function (t) {
      setTimeout((function () {
        t("confirm" === e)
      }), 1e3)
    }))
  },
  prew: function (e) {
    var a = this;
    return n(t().mark((function n() {
      var i, s;
      return t().wrap((function (t) {
        for (;;) switch (t.prev = t.next) {
          case 0:
            i = e.currentTarget.dataset.id, 0 == (s = a.data.list[i]).OldId ? wx.navigateTo({
              url: "/pages/views/detail/index?id=" + s.ID
            }) : r.default.confirm({
              title: "提示",
              confirmButtonText: "查看素材",
              cancelButtonText: "查看详情",
              closeOnClickOverlay: !0,
              beforeClose: function (e) {
                return "confirm" === e ? wx.navigateTo({
                  url: "/pages/caseMarket/detail/page?id=" + s.OldId
                }) : "cancel" === e && wx.navigateTo({
                  url: "/pages/views/detail/index?id=" + s.ID
                }), !0
              }
            }).then((function () {})).catch((function (e) {}));
          case 3:
          case "end":
            return t.stop()
        }
      }), n)
    })))()
  },
  detailClick: function (e) {
    var t = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/pages/views/detail/index?id=" + t
    })
  },
  bindViewTap: function () {},
  onLoad: function (e) {
    var r = this;
    return n(t().mark((function n() {
      var a;
      return t().wrap((function (t) {
        for (;;) switch (t.prev = t.next) {
          case 0:
            return t.next = 2, i();
          case 2:
            a = +e.id || 0, r.setData({
              id: a,
              count: "",
              list: []
            }, (function () {
              r.scroll()
            }));
          case 4:
          case "end":
            return t.stop()
        }
      }), n)
    })))()
  },
  scroll: function () {
    var e = this;
    return n(t().mark((function n() {
      var r, a, i;
      return t().wrap((function (t) {
        for (;;) switch (t.prev = t.next) {
          case 0:
            return r = e.data, (a = Object.create(r)).list = [], t.next = 5, s("GetUserCaseList", a);
          case 5:
            (i = t.sent).Data.length > 0 ? (r.page++, r.list = r.list.concat(i.Data), r.count = i.Msg, e.setData(r)) : wx.showToast({
              title: "暂无数据",
              icon: "none"
            });
          case 7:
          case "end":
            return t.stop()
        }
      }), n)
    })))()
  },
  sortClick: function (e) {
    var r = this;
    return n(t().mark((function n() {
      var a, i, o, c, u, l;
      return t().wrap((function (t) {
        for (;;) switch (t.prev = t.next) {
          case 0:
            return a = +e.currentTarget.dataset.ind, i = r.data.list, o = i[a], t.next = 5, wx.showModal({
              placeholderText: "请输入排序值,越小越靠前",
              editable: !0,
              content: String(o.SortCode)
            });
          case 5:
            if (!(c = t.sent).confirm) {
              t.next = 14;
              break
            }
            return o.SortCode = Number(c.content), t.next = 10, s("SetUserCaseSort", o);
          case 10:
            u = t.sent, l = u.Msg, wx.showToast({
              title: l
            }), r.setData({
              page: 0,
              list: []
            }, (function () {
              r.scroll()
            }));
          case 14:
          case "end":
            return t.stop()
        }
      }), n)
    })))()
  },
  remove: function (e) {
    var r = this;
    return n(t().mark((function n() {
      var a, i, o, c;
      return t().wrap((function (t) {
        for (;;) switch (t.prev = t.next) {
          case 0:
            return a = +e.currentTarget.dataset.ind, i = r.data.list, o = i[a], t.next = 5, wx.showModal({
              content: "即将删除素材:" + o.Name + ",是否继续?"
            });
          case 5:
            if (!t.sent.confirm) {
              t.next = 13;
              break
            }
            return t.next = 9, s("RemoveUserCase", o);
          case 9:
            c = t.sent, c.Msg, i.splice(a, 1), r.setData({
              list: i
            });
          case 13:
          case "end":
            return t.stop()
        }
      }), n)
    })))()
  },
  onReachBottom: function () {
    var e = this;
    setTimeout((function () {
      e.scroll()
    }), 300)
  }
});