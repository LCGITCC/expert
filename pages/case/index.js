var e = require("../../@babel/runtime/helpers/regeneratorRuntime"),
  t = require("../../@babel/runtime/helpers/asyncToGenerator"),
  a = require("../../utils/$"),
  n = a.GetUser,
  r = a.post,
  o = a.uploadimg;
getApp();
Page({
  data: {
    share: !1
  },
  bindViewTap: function () {},
  onLoad: function () {
    var a = this;
    return t(e().mark((function t() {
      var o, s;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return e.next = 2, n();
          case 2:
            return e.next = 4, r("Index", {});
          case 4:
            return o = e.sent, a.setData(o.Data), e.next = 8, r("GetExtShop");
          case 8:
            s = e.sent, a.setData({
              list: s.Data
            });
          case 10:
          case "end":
            return e.stop()
        }
      }), t)
    })))()
  },
  jumpTo: function (e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.url
    })
  },
  cloneshop: function (a) {
    var n, o = this;
    wx.showModal({
      title: "提示",
      placeholderText: "请输入店铺ID",
      editable: !0,
      complete: (n = t(e().mark((function t(a) {
        var n;
        return e().wrap((function (e) {
          for (;;) switch (e.prev = e.next) {
            case 0:
              if (!a.confirm) {
                e.next = 10;
                break
              }
              if (console.log("确定", a.content), !a.content) {
                e.next = 10;
                break
              }
              if (a.content != o.data.u.ID) {
                e.next = 6;
                break
              }
              return wx.showModal({
                title: "提示",
                content: "请勿克隆自己的店铺",
                showCancel: !1
              }), e.abrupt("return");
            case 6:
              return e.next = 8, r("GetUserNameById", {
                id: Number(a.content)
              });
            case 8:
              0 == (n = e.sent).Code && (n.Data.IsClone ? wx.navigateTo({
                url: "/pages/case/closeshop/index?id=" + a.content
              }) : wx.showModal({
                title: "提示",
                content: "此店铺不允许克隆",
                showCancel: !1
              }));
            case 10:
            case "end":
              return e.stop()
          }
        }), t)
      }))), function (e) {
        return n.apply(this, arguments)
      })
    })
  },
  scanClick: function (e) {
    var t = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: "/pages/userscan/page?id=" + t
    })
  },
  copyClick: function (e) {
    var t = e.currentTarget.dataset.id;
    console.log(this.data.u), wx.setClipboardData({
      data: t.toString(),
      success: function (e) {
        console.log("调用成功", e)
      },
      fail: function (e) {
        console.log("调用失败", e)
      }
    })
  },
  wxcode: function (a) {
    var n = this;
    return t(e().mark((function t() {
      var a, o, s;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return a = n.data.u, "pages/views/tabbar/page", o = a.ID + "", e.next = 5, r("wxcode", {
              scene: o,
              page: "pages/views/tabbar/page"
            });
          case 5:
            s = e.sent, wx.previewImage({
              urls: [s.Data]
            });
          case 7:
          case "end":
            return e.stop()
        }
      }), t)
    })))()
  },
  onPullDownRefresh: function (e) {
    var t = this;
    setTimeout((function () {
      wx.stopPullDownRefresh(), t.onLoad(t.options)
    }), 300)
  },
  showTap: function () {
    this.setData({
      share: !this.data.share
    })
  },
  settitle: function (e) {
    var t = this.data.form || {};
    t.title = e.detail.value, this.setData({
      form: t
    })
  },
  upload: function (a) {
    var n = this;
    return t(e().mark((function t() {
      var a, r;
      return e().wrap((function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return e.next = 2, o();
          case 2:
            a = e.sent, (r = n.data.form || {}).url = a, n.setData({
              form: r
            });
          case 6:
          case "end":
            return e.stop()
        }
      }), t)
    })))()
  },
  onShareAppMessage: function (e) {
    var t = this.data.u;
    console.log(e);
    var a = this.data.form || {},
      n = a.title || "",
      r = a.url || "";
    if ("button" == e.from && t) return {
      title: n,
      path: "/pages/views/tabbar/page?id=" + t.ID,
      imageUrl: r
    }
  }
});